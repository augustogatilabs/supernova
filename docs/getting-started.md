# Getting started with Supernova

Para montar o ambiente pode ser usado o Docker incluso ou executar os comandos manualmente.

## Usando Docker

* ```docker-compose up```
* ```docker-compose exec db /docker-entrypoint-initdb.d/createExtension.sh```
* ```docker-compose run api yarn```
* ```docker-compose run app yarn```
* ```docker-compose restart```
* ```docker-compose exec api yarn sequelize db:create```
* ```docker-compose restart```
* ```docker-compose exec api node src/generators/syncDatabase.js```
* ```docker-compose exec api yarn sequelize db:seed:all```

Pronto, sua aplicação estará rodando em [http://localhost:1234](http://localhost:1234) e a API em [http://localhost:3000](http://localhost:3000).

## Manualmente ou Gitpod

### APP (front-end)

Para iniciar o front-end da aplicação, em um novo terminal acesse a pasta ```/app``` e execute:
* ```yarn``` para instalar as dependências. Usado apenas ao iniciar o projeto ou ao adicionar novas dependências
* ```yarn dev``` irá iniciar o servidor de desenvolvimento Parcel

Pronto, sua aplicação estará rodando em [http://localhost:1234](http://localhost:1234).

### API

A API é construída em Node com Express e Sequelize para acesso ao banco de dados PostgreSQL. Acessando a pasta ```/api``` temos uma imagem Docker para o banco de dados.

* ```cd api```
* ```cp .env.dist .env``` - Estas são as variáveis de ambiente, se seu banco de dados tem outros dados para conexão, aqui que deverão ser alterados. Se usar a imagem acima, está OK
* Edite o arquivo ```.env``` com as informações necessárias para conexão do banco, se estiver usando Gitpod: ```DEV_DB_USER=gitpod``` e ```DEV_DB_PASSWORD=postgres```.
* ```yarn``` para instalar dependências
* ```yarn sequelize db:create``` irá criar a base de dados
* ```POSTGRES_DB=database_development ../docker/postgres/createExtension.sh``` irá configurar o banco de dados (criar extensão UUID)
* ```node src/generators/syncDatabase.js``` para atualizar a estrutura de banco de dados (pode demorar alguns segundos)
* ```yarn sequelize db:seed:all``` para atualizar a estrutura de banco de dados (pode demorar alguns segundos)
* ```yarn dev``` irá iniciar o servidor de desenvolvimento

A API está pronta e pode ser acessada em [http://localhost:3000](http://localhost:3000).

## Começando a usar

Depois destes passos você pode acessar o front-end da aplicação em [http://localhost:1234/#/admin/login](http://localhost:1234/#/admin/login) e acessar com os dados:
* E-mail: ```admin@supernova.test```
* Senha: ```admin123```
