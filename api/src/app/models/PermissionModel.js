module.exports = (sequelize, DataTypes) => {
  const PermissionModel = sequelize.define("PermissionModel", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    action: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  PermissionModel.associate = (models) => {
    PermissionModel.belongsTo(models.Role);
    PermissionModel.belongsTo(models.Model);
  };

  return PermissionModel;
};
