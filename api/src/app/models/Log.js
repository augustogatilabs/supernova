module.exports = (sequelize, DataTypes) => {
  const Log = sequelize.define("Log", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    statusCode: DataTypes.INTEGER,
    modelName: DataTypes.STRING,
    function: DataTypes.STRING,
    method: DataTypes.STRING,
    timing: DataTypes.DECIMAL(12, 2),
    query: DataTypes.JSON,
    path: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  Log.associate = (models) => {
    Log.belongsTo(models.User);
  };

  return Log;
};
