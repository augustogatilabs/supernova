module.exports = (sequelize, DataTypes) => {
  const ViewRowAction = sequelize.define("ViewRowAction", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    position: DataTypes.STRING,
    class: DataTypes.STRING,
    params: DataTypes.JSON,
    type: DataTypes.STRING,
    order: DataTypes.STRING,
    labelKey: DataTypes.STRING,
    slug: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ViewRowAction.associate = (models) => {
    ViewRowAction.belongsTo(models.View);
  };

  return ViewRowAction;
};
