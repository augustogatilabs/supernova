module.exports = (sequelize, DataTypes) => {
  const View = sequelize.define("View", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    showOnMenu: DataTypes.BOOLEAN,
    labelKey: DataTypes.STRING,
    filters: DataTypes.JSON,
    key: DataTypes.STRING,
    type: DataTypes.STRING,
    include: DataTypes.JSON,
    slug: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  View.associate = (models) => {
    View.hasMany(models.ViewRowAction);
    View.hasMany(models.ViewAction);
    View.hasMany(models.ViewField);
    View.belongsTo(models.Model);
  };

  return View;
};
