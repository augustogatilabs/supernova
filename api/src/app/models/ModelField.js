module.exports = (sequelize, DataTypes) => {
  const ModelField = sequelize.define("ModelField", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    name: DataTypes.STRING,
    default: DataTypes.STRING,
    targetModel: DataTypes.STRING,
    targetModelAlias: DataTypes.STRING,
    length: DataTypes.INTEGER,
    modelFieldType: DataTypes.STRING,
    selectOptions: DataTypes.JSON,
    type: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ModelField.associate = (models) => {
    ModelField.hasMany(models.ViewField);
    ModelField.belongsTo(models.Model);
  };

  return ModelField;
};
