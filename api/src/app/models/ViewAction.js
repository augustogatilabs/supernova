module.exports = (sequelize, DataTypes) => {
  const ViewAction = sequelize.define("ViewAction", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    class: DataTypes.STRING,
    slug: DataTypes.STRING,
    type: DataTypes.STRING,
    labelKey: DataTypes.STRING,
    order: DataTypes.STRING,
    params: DataTypes.JSON,
    position: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ViewAction.associate = (models) => {
    ViewAction.belongsTo(models.View);
  };

  return ViewAction;
};
