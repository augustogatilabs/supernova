const { User } = require('../models');

const sessionController = {
  async store (req, res) {
    const { email, password } = req.body;

    let messages = []
    if (!email) {
      messages.push('E-mail not provided')
    }

    if (!password) {
      messages.push('Password not provided')
    }

    if (messages.length) {
      return res.status(401).json({ messages });
    }

    const user = await User.findOne({ where: { email } });

    if (!user) {
      return res.status(401).json({ messages: ['User not found'] });
    }

    if (!(await user.checkPassword(password))) {
      return res.status(401).json({ messages: ['Incorrect password'] });
    }

    return res.json({
      user,
      token: user.generateToken()
    });
  }
}

module.exports = sessionController
