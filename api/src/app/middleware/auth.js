const jwt = require('jsonwebtoken')
const { promisify } = require('util')
const { User } = require('../models')

module.exports = async (req, res, next) => {
  const authHeader = req.headers.authorization || ''
  const isMetadataOrLogin = req.originalUrl === '/api/metadata' || req.originalUrl === '/api/extract-metadata' || req.originalUrl === '/api/sessions' || req.originalUrl.startsWith('/public')

  if (!authHeader && !isMetadataOrLogin) {
    return res.status(401).json({ message: 'Token not provided' })
  }

  const [, token] = authHeader.split(' ')

  try {
    const decoded = await promisify(jwt.verify)(token, process.env.APP_SECRET)
    await User.findByPk(decoded.id)

    req.userId = decoded.id

    return next()
  } catch (err) {
    if (isMetadataOrLogin) {
      return next()
    }

    return res.status(401).json({ message: 'Invalid token' })
  }
}
