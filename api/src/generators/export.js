const fs = require('fs').promises
const path = require('path')
const prettier = require("prettier")

// Paths
const metadataPath = path.resolve(__dirname, '..', 'app', 'metadata')

let models = require('../app/models')
const { Model } = models

async function exportMetadata () {
  try {
    await fs.mkdir(metadataPath)
  } catch (e) {}

  const meta = await Model.findAll({
    include: [
      models['ModelField'],
      {
        model: models['View'],
        include: [
          models['ViewField'],
          models['ViewAction'],
          models['ViewRowAction'],
        ],
      },
    ],
  })


  try {
    await meta.forEach(async model => {
      const filePath = path.resolve(metadataPath, `${model.modelName}.json`)
      const fileBuffer = JSON.stringify(model, null, 2)
      const pretty = prettier.format(fileBuffer, { parser: 'json' })
      await fs.writeFile(filePath, pretty)
    })
  } catch (e) {
    console.error(e)
  }
}

process.argv.forEach(val =>  {
  if (val === '-s') {
    exportMetadata()
  }
})

module.exports = {
  exportMetadata
}
