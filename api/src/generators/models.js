const fs = require('fs').promises
const path = require('path')
const prettier = require("prettier")

// Paths
const modelsPath = path.resolve(__dirname, '..', 'app', 'models')
const metadataPath = path.resolve(__dirname, '..', 'app', 'metadata')
const modelTemplatePath = path.resolve(__dirname, '..', 'templates', 'model.js.tpl')

const modelsNotToUpdate = [
  // !important! User should not be updated because of hooks and custom functions...
  'User', 
  // There is a case with ChildViewId that should be resolved to work properly
  'ViewField',
  // There is a case with defaultValue that should be resolved to work properly
  'Language',
]

async function init () {
  return new Promise(async (resolve) => {
    const metaFiles = await fs.readdir(metadataPath)
    const modelsToUpdate = metaFiles.filter(fileName => 
      modelsNotToUpdate.includes(fileName.replace('.json', '')) === false)

    const modelTemplateFile = await fs.readFile(modelTemplatePath, 'utf-8')

    Promise.all(
      modelsToUpdate.map(async (metaFile) => {
        return new Promise(async () => {
          const file = await fs.readFile(path.resolve(metadataPath, metaFile), 'utf-8')
          const json = JSON.parse(file)
          const fields = []
          const relations = []
          json.ModelFields
            .filter(field => !['createdAt', 'updatedAt', 'deletedAt'].includes(field.name))
            .forEach(field => {
              if (['belongsTo', 'hasMany'].includes(field.type)) {
                relations.push(`{#modelName}.${field.type}(models.${field.targetModel});`)
              } else {
                fields.push(`${field.name}: ${field.modelFieldType},`)
              }
            })

          let parsed = modelTemplateFile
          parsed = parsed.replace(/\{#fields\}/g, fields.join(''))
          parsed = parsed.replace(/\{#relations\}/g, relations.join(''))
          parsed = parsed.replace(/\{#modelName\}/g, json.modelName)

          const pretty = prettier.format(parsed, { parser: 'babel' })
          await fs.writeFile(path.resolve(modelsPath, `${json.modelName}.js`), pretty)
          resolve()
        })
      })
    ).then(resolve)
  })
}

init()
