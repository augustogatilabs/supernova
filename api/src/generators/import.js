const fs = require('fs').promises
// const util = require('util')
const path = require('path')
const { Model, ModelField, View, ViewField, ViewAction, ViewRowAction } = require('../app/models')

const metadataPath = path.resolve(__dirname, '..', 'app', 'metadata')

// If you want to filter out models
const modelsNotToUpdate = [
]

async function init () {
  const metaFiles = await fs.readdir(metadataPath)
  const modelsToUpdate = metaFiles.filter(fileName => modelsNotToUpdate.includes(fileName.replace('.json', '')) === false)

  try {
    await Promise.all(
      modelsToUpdate.map(async (metaFile) => {
        const file = await fs.readFile(path.resolve(metadataPath, metaFile), 'utf-8')
        const model = JSON.parse(file)

        // Model
        await Model.upsert(model)

        await Promise.all(
          [
            // ModelFields
            ...(model.ModelFields ? model.ModelFields.map(async (modelField) => {
              await ModelField.upsert(modelField)
            }) : []),

            // Views
            ...(model.Views ? model.Views.map(async (view) => {
              await View.upsert(view)
  
              await Promise.all(
                [
                  // ViewFields
                  ...(view.ViewFields ? view.ViewFields.map(async (viewField) => {
                    await ViewField.upsert(viewField)
                  }) : []),

                  // ViewActions
                  ...(view.ViewActions ? view.ViewActions.map(async (viewAction) => {
                    await ViewAction.upsert(viewAction)
                  }) : []),

                  // ViewRowActions
                  ...(view.ViewRowActions ? view.ViewRowActions.map(async (viewRowAction) => {
                    await ViewRowAction.upsert(viewRowAction)
                  }) : []),
                ]
              )
            }) : []),
          ]
        )
      })
    )
  } catch (e) {
    console.error(e)
    throw new Error(e)
  }
}

process.argv.forEach(val =>  {
  if (val === '-s') {
    init()
  }
})

module.exports = {
  init
}
